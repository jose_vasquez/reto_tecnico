import axios from 'axios';
import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {Container} from 'reactstrap';
import Swal from 'sweetalert2';
class Division extends Component{

    state ={
        Division: [],
        loading: true
    }

    async componentDidMount(){
        const res = await axios.get('http://localhost:8000/api/divisiones');
        console.log(res);
        if(res.data.status === 200){
            this.setState({
                Division: res.data.divisiones,
                loading: false
            });
        }
        
    }

    deleteDivision =  async (e,id) =>{
        const thisClickedFunda = e.currentTarget;
        thisClickedFunda.innerText = "Eliminando";
        const res = await axios.delete(`http://localhost:8000/api/delete-division/${id}`);
        
        if(res.data.status === 200){
            Swal.fire({
                title: 'Eliminado!',
                text: res.data.message,
                icon: 'success',
                confirmButtonText: 'OK!'
            });
            thisClickedFunda.closest("tr").remove();
            console.log(res.data.message);
        }

    } 
    render(){
        var division_HTMLTABLE = "";
        if(this.state.loading){
            division_HTMLTABLE = <tr><td colSpan="8"><h2>Cargando ...</h2></td></tr>
        }else{
            division_HTMLTABLE =
            this.state.Division.map( (item) => {
                return (
                    <tr key = {item.id}>
                        <td>{item.division}</td>
                        <td>{item.division_superior}</td>
                        <td>{item.colaboradores}</td>
                        <td>{item.nivel}</td>
                        <td>{item.subdivisiones}</td>
                        <td>{item.embajadores}</td>
                        <td>
                            <Link to={`edit-division/${item.id}`} className="btn btn-success btn-sm">Editar</Link>
                        </td>
                        <td>
                            <button type="button" onClick={(e) => this.deleteDivision(e,item.id)} className="btn btn-danger btn-sm">Eliminar</button>
                        </td>
                    </tr>
                );
            } );
        }
        
        return(
        <>
        <Container>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="card">
                            <div className="card-header">
                                <h4>Organización
                                    <Link to={'add-division'} className="btn btn-primary btn-sm float-end">Agregar</Link>
                                </h4>

                            </div>
                            <div className="card-body">
                                <table className="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Division</th>
                                            <th>Division Superior</th>
                                            <th>Colaboradores</th>
                                            <th>Nivel</th>
                                            <th>Subdivisiones</th>
                                            <th>Embajadores</th>
                                            <th>Editar</th>
                                            <th>Eliminar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {division_HTMLTABLE}
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                    
                </div>

            </div>
        </Container>     
        </>    
        );
    }
}
export default Division;
