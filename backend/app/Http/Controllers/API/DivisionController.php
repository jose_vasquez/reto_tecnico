<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Division;

class DivisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $divisiones = Division::all();
        //return $divisiones;
        return response() -> json([
            'status' => 200,
            'divisiones' => $divisiones,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*$validator = Validator::make($request->all(),[
            'division' => 'required|max:191',
            'division_superior' => 'required|max:191',
            'colaboradores' => 'required|numeric|max:5',
            'nivel' => 'required|numeric|max:5',
            'subdivisiones' => 'required|numeric|max:5',
            'embajadores' => 'required|max:191',     

        ]);*/

        /*if($validator->fails()){
            return response() -> json([
                'validation_err' => $validator->messages(),
            ]);
        }else{*/
            $division = new Division();
            $division->division = $request -> input('division');
            $division->division_superior = $request -> input('division_superior');
            $division->colaboradores = $request -> input('colaboradores');
            $division->nivel = $request -> input('nivel');
            $division->subdivisiones = $request -> input('subdivisiones');
            $division->embajadores = $request -> input('embajadores');
    
            $division -> save();
    
            return response() -> json([
                'status' => 200,
                'message' => 'Division Agregado Exitosamente',
            ]);
        /*}*/
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $division = Division::find($id);
        return response() ->json([
            'status' => 200,
            'division' => $division,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $division  = Division::findOrFail($request->id);
        $division->division = $request -> division;
        $division->division_superior = $request -> division_superior;
        $division->colaboradores = $request -> colaboradores;
        $division->nivel = $request -> nivel;
        $division->subdivisiones = $request -> subdivisiones;
        $division->embajadores = $request -> embajadores;

        $division ->save();

        return response() -> json([
            'status' => 200,
            'message' => 'Division Actualizado Exitosamente',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::find($id);
        $division->delete();
        return response() -> json([
            'status' => 200,
            'message' => 'Division Eliminado Exitosamente',
        ]);
    }
}
