import React from 'react';
import { BrowserRouter as Router,Routes,Route} from 'react-router-dom';

import Division from './components/Division';
import AddDivision from './components/AddDivision';
import EditDivision from './components/EditDivision';

function App() {
  return (
    <Router>
      <Routes>
        <Route exact path="/" element={<Division />}/>
        <Route path="/add-division" element={<AddDivision />}/>
        <Route path="/edit-division/:id" element={<EditDivision />}/>
      </Routes>
    </Router>
  );
}

export default App;
