import axios from 'axios';
import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

class AddDivision extends Component
{
    constructor(props) {
        super(props)

        this.state = {
            division: '',
            division_superior: '',
            colaboradores: '',
            nivel: '',
            subdivisiones: '',
            embajadores: '',
            error_list: [],
        }
        this.setDivision = this.setDivision.bind(this);
        this.setDivisionS = this.setDivisionS.bind(this);
        this.setColaboradores = this.setColaboradores.bind(this);
        this.setNivel = this.setNivel.bind(this);
        this.setSubDivisiones = this.setSubDivisiones.bind(this);
        this.setEmbajadores = this.setEmbajadores.bind(this);
    }
    /*state = {
        division: '',
        division_superior: '',
        colaboradores: '',
        nivel: '',
        subdivisiones: '',
        embajadores: '',
    }*/
    setDivision(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            division:e.target.value
        })
    }
    setDivisionS(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            division_superior:e.target.value
        })
    }

    setColaboradores(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            colaboradores:e.target.value
        })
    }

    setNivel(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            nivel:e.target.value
        })
    }

    setSubDivisiones(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            subdivisiones:e.target.value
        })
    }

    setEmbajadores(e){
        e.preventDefault();
        //console.log(e.target.value);
        this.setState({
            embajadores:e.target.value
        })
    }


    saveDivision = async (e) => {
        e.preventDefault();

        const res = await axios.post('http://localhost:8000/api/add-division',this.state);
        if(res.data.status === 200){
            //console.log(res.data.message);
            Swal.fire({
                title: 'Guardado!',
                text: res.data.message,
                icon: 'success',
                confirmButtonText: 'OK!'
            });
            this.setState({
                division: '',
                division_superior: '',
                colaboradores: '',
                nivel: '',
                subdivisiones: '',
                embajadores: '',
            });
        }/*else{
            this.setState({
                error_list: res.data.validation_err,
            });
        }*/
    }

    render(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header">
                                <h4>Agregar División
                                    <Link to={'/'} className="btn btn-primary btn-sm float-end">Back</Link>
                                </h4>

                            </div>
                            <div className="card-body">
                                <form onSubmit={this.saveDivision}>
                                    <div className="form-group mb-3">
                                        <label>División</label>
                                        <input type ="text" name="division"  onChange={this.setDivision} value={this.state.division} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.division}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>División Superior</label>
                                        <input type ="text" name="division_superior"  onChange={this.setDivisionS} value={this.state.division_superior} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.division_superior}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Colaboradores</label>
                                        <input type ="number" name="colaboradores"  onChange={this.setColaboradores} value={this.state.colaboradores} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.colaboradores}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Nivel</label>
                                        <input type ="number" name="nivel"  onChange={this.setNivel} value={this.state.nivel} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.nivel}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Subdivisiones</label>
                                        <input type ="number" name="subdivisiones"  onChange={this.setSubDivisiones} value={this.state.subdivisiones} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.subdivisiones}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <label>Embajadores</label>
                                        <input type ="text" name="embajadores"  onChange={this.setEmbajadores} value={this.state.embajadores} className="form-control"/>
                                        <span className="text-danger">{this.state.error_list.embajadores}</span>
                                    </div>
                                    <div className="form-group mb-3">
                                        <button type="submit" className="btn btn-primary"> Guardar división</button>
                                    </div>
                                    
                                </form>

                            </div>

                        </div>
                    </div>
                    
                </div>

            </div>
        );
    }
}
export default AddDivision;
