<?php

use App\Http\Controllers\API\DivisionController;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/divisiones',[DivisionController::class,'index']);
Route::post('/add-division',[DivisionController::class,'store']);
Route::get('/edit-division/{id}',[DivisionController::class,'edit']);
Route::put('/update-division/{id}',[DivisionController::class,'update']);
Route::delete('/delete-division/{id}',[DivisionController::class,'destroy']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/*
Route::get('/divisiones','App\Http\Controllers\DivisionController@index');//mostrar todos los registros
Route::post('/divisiones','App\Http\Controllers\DivisionController@store');//crear un registro
Route::put('/divisiones/{id}','App\Http\Controllers\DivisionController@update');//actualizar un registro
Route::delete('/divisiones/{id}','App\Http\Controllers\DivisionController@destroy');//eliminar un registro
*/
