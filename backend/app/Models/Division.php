<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Division extends Model
{
    use HasFactory;
    protected $table = 'divisiones';
    protected $filelable = ['division','division_superior','colaboradores','nivel','subdivisiones','embajadores'];
}
